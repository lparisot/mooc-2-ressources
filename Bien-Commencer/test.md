essai

Pour vous entrainer vous pouvez faire [ce tutoriel en français] (https://www.markdowntutorial.com/fr/)

On peut insister sur une partie de texte : **ceci est important** et d'ailleurs __cela aussi__

Bref 2 caractères étoile * ou alors 2 underscore _

Sinon on peut aussi utiliser l'*emphase pour distinguer une portion de texte* ou _comme ça_

# Ceci est un titre de niveau 1

## Ceci est un titre de niveau 2

### Ceci est un titre de niveau 3

- item 1

- item 2

- item 3

* item 1

* item 2

* item 3

1. item 1

2. item 2

3. item 3

$$\frac{\pi}{4} = \lim_{n\rightarrow\infty} \frac{\varphi(n)}{n^2}$$

Des formules plus simples intégrées à la lignes sont possible avec un seul $ comme délimiteur : $\forall n\in\mathbb{N}, n\geq 0$.

| Langage | Inventeur | Année |

| :------ | :-------: | ----: |

| Python | Guido van Rossum | 1991 |

| Java | James Gosling | 1995 |

| aligné à gauche | centré | aligné à droite |

```python
def foo(n):
    return n**2


